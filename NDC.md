# Note de clarification

## Objets

### Utilisateur
- Email (Unique, Non null)
- Identité _(Identité civile)_ (Unique, Non null)
- Identifiant (Unique, Non null)
- Mot de passe (Non null)
- Age _(Entier calculé)_
- Genre
- Pays
- Poste
- Entreprise _(Entreprise)_
- Numéros de téléphones
- Adresse (Numéro et rue)

### Identité civile
- Nom (Non null)
- Date de naissance (Non null)
- Ville (Non null)
- Prénom (Non null)
  - Clé / Unique (Nom, Date de naissance, Ville)

### Entreprise 
- Siret (Unique, Non null)
- Nom entreprise (Unique, Non null)
- Secteur (Non null)
- Description
- Statut juridique
- Ville siège
- Effectif sur l'application _(Entier calculé)_

### Flux de publication
- Titre du flux (Unique, Non null)
- Responsable _(Utilisateur)_ (Non null)
- Date de création (Non null)
- Confidentialité _(Catégorie de confidentialité)_ (Non null)
    - Possède au minimum deux groupes utilisateur : "Rédacteur" et "Lecteur"

### Catégorie de confidentialité
- Nom (Unique, Non null)
- Description

### Publication (Article)
- Titre (Non null)
- Flux de publication parent _(Flux de publication)_ (Non null)
- Date de création (Non null)
- Date de publication
- Chemin de la vignette
- Créateur _(Utilisateur)_ (Non null)
- Rédacteur _(Utilisateur)_ (Non null)
- État _(États possibles)_ (Non null)
- Score _(Entier calculé)_
- Sujet _(Sujet)_ (Non null)
  - Clé / Unique (Titre, Date de création)

### Sujet
- Nom (Unique, Non null)
- Description (Unique)

### Groupe d'utilisateur
- Nom du groupe (Unique, Non null)
- Description
- Responsable _(Utilisateur)_ (Non null)
- Permissions _(Liste de permissions)_
- Flux de publication parent (inhérent) _(Flux de publication)_
- Est lié à 0 ou plusieurs flux de publication **si non inhérant**

### Commentaire
- Auteur _(Utilisateur)_ (Non null)
- Date (Non null)
- Contenu (Non null)
- Publication concernée _(Publication)_ (Non null)
  - Unique (Auteur, Publication concernée, Date)


### Vote
- Auteur _(Utilisateur)_ (Non null)
- Date (Non null)
- Positif _(Boolean)_ (Non null)
- Publication concernée _(Publication)_ (Non null)
  - Unique (Auteur, Publication concernée)

### États possibles
- Nom (Unique, Non null)
- Description
- Publication visible ? (Non null)

### Paramètre de l'application
- Nom paramètre (Unique, Non null)
- Valeur (Non null)

### Permissions
- Nom (Unique, Non null)
- Code (Unique, Non null)

## Contraintes

### Permissions

- Un rédacteur peut modifier ou supprimer les publications qu'il a lui-même créées. Il en devient le **créateur**.
- Une publication dans l'**état** "validé" ne peut être modifiée.
- L'initiateur d'une modification sur un document en devient le rédacteur.

- Un flux de publication peut être créé par tout utilisateur qui en devient automatiquement responsable.
- Le rôle de responsable de flux de publication peut être cédé à un autre utilisateur.
- Le responsable flux peut gérer les groupes "Rédacteur" et "Lecteur".
- Parlant d'un flux de publication, la permission de lecture du groupe utilisateur "Lecteur" est incluse dans le groupe utilisateur "Rédacteur", qui est le rôle de permission dominant. (lecture + écriture = écriture)

- Il existe trois niveau de confidentialité :
    - Publique, tout le monde peut lire les publications sur le flux
    - Interne, tout les utilisateurs de la même entreprise que le responsable du flux peuvent lire
    - Privé, seul les groupes mentionnées sur le flux on des droits

- Tout utilisateur peut créer un groupe utilisateur dont il devient automatiquement le responsable.
- Le responsable d'un groupe d'utilisateur peut ajouter ou supprimer des utilisateurs.
- Le responsable d'un groupe d'utilisateur peut transférer son rôle de responsable à une autre utilisateur.
- Les permissions sont codées avec une suite de 0 et de 1, symbolisant tour a tour si la permission à l'index i est vraie ou fausse. Exemple : 11000011 = Je peux lire, je peux écrire, je ne peux pas valider, je ne peux pas rejeter, je ne peux pas supprimer, je ne peux pas modifier, je peux voter, je peux commenter


### Autres

- Deux utilisateurs ne peuvent pas exister avec le même mail
- Deux utilisateurs ne peuvent pas exister avec le même identifiant
- L'âge est calculé par la date de naissance si cela est possible.
- On peut calculer le nombre d'utilisateur d'une entreprise sur l'application
- Une publication à une date de création, qui correspond à la date à laquelle la publication a été créée.
- Une publication à une date de publication, qui correspond à la date à laquelle la publication à été validée.
- On peut calculer le score d'une publication à partir des votes concernant ce dernier.
- Une image peut être associé à une publication par un chemin vers un fichier image.
- Les flux de publications sont par défaut triés du plus récent au plus ancien.
- On peut trier les flux de publication par date de création
- Le tri des flux de publications propose une option pour trier du score le plus élevé au plus bas.
- On peut filtrer les publications par sujets.
- Chaque utilisateur présent dans l'un des groupes utilisateur d'un flux peut poster des commentaires **sur une publication**. Ces derniers sont triés du plus anciens au plus récent. (Ordre chronologique)

### Frontend

- Chaque utilisateur possède une page "home" ou "profile". Elle affiche :
    - Nom de l'utilisateur
    - Prénom de l'utilisateur
    - Email de l'utilisateur
    - Groupes auxquels appartient l'utilisateur
    - Un dashboard lui regroupant tous les flux qu'il peut lire ( appartenance au groupe "Lecteur")
- Une page d'administration de flux est visible par les rédacteurs de ce même flux et sert à rajouter une publication au flux.
- Un article n'est visible qu'une fois validé (état = validé).



## Clarifications

- Un groupe d'utilisateur possédant un ensemble de permissions, on aurait pu faire le choix de rendre cet objet inhérent à une groupe d'utilisateur, et donc non réutilisable ailleurs dans l'application. Nous avons tout de fois fais le choix de garder la multiplicité des liens entre groupe utilisateur et flux de publication. On estime qu'il est souhaitable d'avoir un groupe administrateur que l'on peut greffer à n'importe quel flux par exemple.
- La liste des permissions est la suivante
    - lecture (toute publication)
    - écriture (création)
    - valider / dévalider (n'importe quelle publication)
    - rejeter (n'importe quelle publication)
    - supprimer (n'importe quelle publication)
    - modifier (n'importe quelle publication)
    - voter (n'importe quelle publication)
    - commenter (n'importe quelle publication)
- Les groupes "Rédacteur" et "Lecteur" sont générés automatiquement à la création d'un flux de publication. Ils sont notés comme inhérant au flux de publication et ont par défaut et de manière immuable les droits respectifs de rédaction et de lecture.
- Un groupe d'utilisateur inhérent à un flux de publication ne peut pas être ajouté à un autre flux de publication par un utilisateur.
- Le seul paramètre de l'application qui a un impact est "score_minimal". On pourra cependant étendre les possibilités dans le futur.
- Le score minimal est forcément un entier négatif, sans quoi toute publication serait rejetée dès sa création.
- Un commentaire peut être créé par tout utilisateur qui en a le droit mais il ne peut être modifié.
- Seul le responsable d'un flux de publication peut supprimer un commentaire
