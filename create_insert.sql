-- START SQL --

DROP TABLE IF EXISTS Publication__Sujet cascade;
DROP TABLE IF EXISTS Flux_de_publication__Groupe_d_utilisateurs cascade;
DROP TABLE IF EXISTS Permissions__Groupe_d_utilisateurs cascade;
DROP TABLE IF EXISTS Utilisateur__Groupe_d_utilisateurs cascade;
DROP TABLE IF EXISTS Parametre_de_l_application cascade;
DROP TABLE IF EXISTS Sujet cascade;
DROP TABLE IF EXISTS Permissions cascade;
DROP TABLE IF EXISTS Commentaire cascade;
DROP TABLE IF EXISTS Vote cascade;
DROP TABLE IF EXISTS Publication cascade;
DROP TABLE IF EXISTS Etats_possibles cascade;
DROP TABLE IF EXISTS Groupe_d_utilisateurs cascade;
DROP TABLE IF EXISTS Flux_de_publication cascade;
DROP TABLE IF EXISTS Categorie_confidentialite cascade;
DROP TABLE IF EXISTS Utilisateur cascade;
DROP TABLE IF EXISTS Identite_civile cascade;
DROP TABLE IF EXISTS Entreprise cascade;

CREATE TABLE Entreprise (
    siret INTEGER PRIMARY KEY,
    nom_ent TEXT UNIQUE NOT NULL,
    secteur_ent TEXT NOT NULL,
    description_ent TEXT,
    statut_juridique TEXT,
    ville_siege TEXT
);

CREATE TABLE Identite_civile (
    nom TEXT, 
    date_de_naissance DATE, 
    ville TEXT, 
    prenom TEXT NOT NULL,
    PRIMARY KEY (nom, date_de_naissance, ville)
);

CREATE TABLE Utilisateur (
    email TEXT PRIMARY KEY, 
    identifiant TEXT UNIQUE NOT NULL,
    nom TEXT NOT NULL,
    mot_de_passe TEXT NOT NULL,
    date_de_naissance DATE NOT NULL,
    genre TEXT,
    pays TEXT,
    ville TEXT NOT NULL,
    numero_et_rue JSON,
    telephones JSON,
    entreprise INTEGER NOT NULL,
    poste TEXT,
    FOREIGN KEY (entreprise) REFERENCES Entreprise(siret),
    FOREIGN KEY (nom, date_de_naissance, ville) REFERENCES Identite_civile(nom, date_de_naissance, ville),
    UNIQUE (nom, date_de_naissance, ville)
);


CREATE TABLE Categorie_confidentialite (
    nom TEXT PRIMARY KEY,  
    description_cat_conf TEXT
);

CREATE TABLE Flux_de_publication (
    titre TEXT PRIMARY KEY, 
    date_creation TIMESTAMP NOT NULL,
    responsable_flux TEXT NOT NULL,
    confidentialite TEXT NOT NULL,
    FOREIGN KEY (responsable_flux) REFERENCES Utilisateur(email),
    FOREIGN KEY (confidentialite) REFERENCES Categorie_confidentialite(nom)
);

CREATE TABLE Groupe_d_utilisateurs (
    nom_groupe TEXT PRIMARY KEY,
    description_groupe TEXT,
    responsable_groupe TEXT NOT NULL,
    flux_inhérent TEXT,
    FOREIGN KEY (responsable_groupe) REFERENCES Utilisateur(email),
    FOREIGN KEY (flux_inhérent) REFERENCES Flux_de_publication(titre)
);

CREATE TABLE Etats_possibles (
    nom_etat TEXT PRIMARY KEY, 
    description_etat TEXT,
    publi_visible BOOLEAN NOT NULL
);

CREATE TABLE Publication (
    id_publi INTEGER PRIMARY KEY,
    titre_publi TEXT NOT NULL,
    date_creation TIMESTAMP NOT NULL,
    date_publi TIMESTAMP,
    chemin_vignette TEXT,
    createur TEXT NOT NULL,
    redacteur TEXT NOT NULL,
    publiee_dans TEXT NOT NULL,
    etat TEXT NOT NULL,
    FOREIGN KEY (createur) REFERENCES Utilisateur(email),
    FOREIGN KEY (redacteur) REFERENCES Utilisateur(email),
    FOREIGN KEY (publiee_dans) REFERENCES Flux_de_publication(titre),
    FOREIGN KEY (etat) REFERENCES Etats_possibles(nom_etat),
    UNIQUE (titre_publi, date_creation)
);

CREATE TABLE Vote (
    votant TEXT NOT NULL,
    concerne INTEGER NOT NULL,
    date_vote TIMESTAMP NOT NULL, 
    positif BOOLEAN NOT NULL,
    FOREIGN KEY (votant) REFERENCES Utilisateur(email),
    FOREIGN KEY (concerne) REFERENCES Publication(id_publi),
    PRIMARY KEY (votant, concerne)
);

CREATE TABLE Commentaire (
    commentateur TEXT NOT NULL,
    concerne INTEGER NOT NULL,
    date_com TIMESTAMP NOT NULL,
    contenu_com TEXT NOT NULL,
    FOREIGN KEY (commentateur) REFERENCES Utilisateur(email),
    FOREIGN KEY (concerne) REFERENCES Publication(id_publi),
    PRIMARY KEY (commentateur, concerne, date_com)
);

CREATE TABLE Permissions (
    nom_perm TEXT PRIMARY KEY,
    code_perm TEXT UNIQUE NOT NULL
);

CREATE TABLE Sujet (
    nom_sujet TEXT PRIMARY KEY,
    description_sujet TEXT UNIQUE
);

CREATE TABLE Parametre_de_l_application (
    nom_parametre TEXT PRIMARY KEY,
    valeur INTEGER NOT NULL
);

CREATE TABLE Utilisateur__Groupe_d_utilisateurs (
    email TEXT,
    nom_groupe TEXT,
    FOREIGN KEY (email) REFERENCES Utilisateur(email),
    FOREIGN KEY (nom_groupe) REFERENCES Groupe_d_utilisateurs(nom_groupe),
    PRIMARY KEY (email, nom_groupe)
);

CREATE TABLE Permissions__Groupe_d_utilisateurs (
    nom_perm TEXT,
    nom_groupe TEXT,
    FOREIGN KEY (nom_perm) REFERENCES Permissions(nom_perm),
    FOREIGN KEY (nom_groupe) REFERENCES Groupe_d_utilisateurs(nom_groupe),
    PRIMARY KEY (nom_perm, nom_groupe)
);

CREATE TABLE Flux_de_publication__Groupe_d_utilisateurs (
    titre TEXT,
    nom_groupe TEXT,
    FOREIGN KEY (titre) REFERENCES Flux_de_publication(titre),
    FOREIGN KEY (nom_groupe) REFERENCES Groupe_d_utilisateurs(nom_groupe),
    PRIMARY KEY (titre, nom_groupe)
);

CREATE TABLE Publication__Sujet (
    id_publi INTEGER,
    nom_sujet TEXT,
    FOREIGN KEY (id_publi) REFERENCES Publication(id_publi),
    FOREIGN KEY (nom_sujet) REFERENCES Sujet(nom_sujet),
    PRIMARY KEY (id_publi, nom_sujet)
);


-- START INSERT --

INSERT INTO Parametre_de_l_application
    VALUES('score_minimal', -20);


INSERT INTO Permissions
    VALUES ('lecteur', '10000000');
INSERT INTO Permissions
    VALUES ('redacteur', '11000000');
INSERT INTO Permissions
    VALUES ('valider', '10100000');
INSERT INTO Permissions
    VALUES ('rejeter', '10010000');
INSERT INTO Permissions
    VALUES ('supprimer', '11001111');
INSERT INTO Permissions
    VALUES ('modifier', '11000111');
INSERT INTO Permissions
    VALUES ('voter', '10000010');
INSERT INTO Permissions
    VALUES ('commenter', '10000001');


INSERT INTO Categorie_confidentialite VALUES ('interne', null);
INSERT INTO Categorie_confidentialite VALUES ('publique', null);
INSERT INTO Categorie_confidentialite VALUES ('prive', null);


INSERT INTO Etats_possibles
    VALUES ('En cours', 'La publication n est pas finie', false);
INSERT INTO Etats_possibles
    VALUES ('Validé', 'La publication est visible', true);
INSERT INTO Etats_possibles
    VALUES ('Rejeté', 'La publication est non conforme', false);


INSERT INTO Sujet
    VALUES ('Energie', 'Electricite, energies renouvlables, energie de demain ...');
INSERT INTO Sujet
    VALUES ('Automobile', 'La mobilité d hier, d aujourd hui, et de demain');
INSERT INTO Sujet
    VALUES ('Application', 'Les applications dans le monde professionnel');
INSERT INTO Sujet
    VALUES ('Organisation', 'Toute publication relatif à l organisation de l entreprise');


INSERT INTO Entreprise 
    VALUES (4842641, 'EDF', 'Energie', 'Fournisseur d electricite', 'SARL', 'Paris');
INSERT INTO Entreprise 
    VALUES (4584561, 'ENGIE', 'Energie', 'Fournisseur d electricite et de gaz', 'SA', 'Paris');
INSERT INTO Entreprise 
    VALUES (5689212, 'RENAULT', 'Automobile', 'Vend des voitures', 'SARL', 'Compiegne');

INSERT INTO Identite_civile
    VALUES ('BERNARD', '1998/05/01', 'Paris', 'Franck');
INSERT INTO Identite_civile
    VALUES ('BEH', '1988/12/01', 'Compiegne', 'Antoine');
INSERT INTO Identite_civile
    VALUES ('VETSON', '1991/07/21', 'Geneve', 'Emma');
INSERT INTO Identite_civile
    VALUES ('LOROUX', '1992/07/19', 'Bordeaux', 'Yasmine');
INSERT INTO Identite_civile
    VALUES ('POFLARD', '1982/03/14', 'Lisieux', 'Lorie');
INSERT INTO Identite_civile
    VALUES ('ROUVELIO', '1972/01/26', 'Pau', 'Arnaud');
INSERT INTO Identite_civile
    VALUES ('DOUVERGNE', '1990/12/22', 'Bruxelles', 'Victor');
INSERT INTO Identite_civile
    VALUES ('CALVIN', '1992/11/07', 'Charleroi', 'Louis');
INSERT INTO Identite_civile
    VALUES ('OYAGAHAÏ', '1987/08/04', 'Lyon', 'Virginie');
INSERT INTO Identite_civile
    VALUES ('ALOMARD', '1984/09/05', 'La Rochelle', 'Julia');

INSERT INTO Utilisateur 
    VALUES ('frank@bernard.fr', 'fbernard', 'BERNARD', '1234', '1998/05/01', 'H', 'France', 'Paris', 
            '{"numero":26, "num_opt":null, "rue": "Rue de la paix"}', '["0699887766", "0155443322"]', 
            4842641, 'Ingénieur');
INSERT INTO Utilisateur 
    VALUES ('antoine@beh.fr', 'abeh', 'BEH', '456', '1988/12/01', 'H', 'France', 'Compiegne', 
            '{"numero":2, "num_opt":"bis", "rue": "Route de Senlis"}', '["0155789565"]', 
            5689212, 'Informaticien');
INSERT INTO Utilisateur 
    VALUES ('emma@vetson.fr', 'evetson', 'VETSON', 'hp874', '1991/07/21', 'F', 'Suisse', 'Geneve', 
            '{"numero":34, "num_opt":null, "rue": "Avenue du Gruyère"}', '["+41284323369"]', 
            4584561, 'Responsable Communication');
INSERT INTO Utilisateur 
    VALUES ('yasmine@loroux.fr', 'yloroux', 'LOROUX', '26BLC45', '1992/07/19', 'F', 'France', 'Bordeaux', 
            '{"numero":7, "num_opt":"ter", "rue": "Boulevard du Vin"}', '["0781224587", "0555621145"]', 
            4842641, 'Génie civil');
INSERT INTO Utilisateur 
    VALUES ('lorie@poflard.fr', 'lpoflard', 'POFLARD', '5342cc', '1982/03/14', 'F', 'France', 'Lisieux', 
            '{"numero":267, "num_opt":null, "rue": "Chemin de la boussole"}', '["0666253998", "0777654578", "0145897633"]', 
            4842641, 'Développeur');
INSERT INTO Utilisateur 
    VALUES ('arnaud@rouvelio.fr', 'arouvelio', 'ROUVELIO', 'Av465b9', '1972/01/26', 'H', 'France', 'Pau', 
            '{"numero":51, "num_opt":null, "rue": "Rue Victor Hugo"}', '["0687153232"]', 
            5689212, 'Responsable recrutement');
INSERT INTO Utilisateur 
    VALUES ('victor@douvergne.fr', 'vdouvergne', 'DOUVERGNE', '9XsUqh8N2', '1990/12/22', 'H', 'Belgique', 'Bruxelles', 
            '{"numero":46, "num_opt":null, "rue": "Avenue des choux"}', '["+32152516959"]', 
            4584561, 'Lead Architecte applicatif');
INSERT INTO Utilisateur 
    VALUES ('louis@calvin.fr', 'lcalvin', 'CALVIN', 'muXp674VB', '1992/11/07', 'H', 'Belgique', 'Charleroi', 
            '{"numero":4, "num_opt":null, "rue": "Route de la bière"}', '["+32187459932"]', 
            4584561, 'Directeur de marque');
INSERT INTO Utilisateur 
    VALUES ('virginie@oyagahai.fr', 'voyagahai', 'OYAGAHAÏ', 'dxiZ25LX2', '1987/08/04', 'F', 'France', 'Lyon', 
            '{"numero":37, "num_opt":"bis", "rue": "Avenue de Grenoble"}', '["0654893265", "0247480230"]', 
            4584561, 'Responsable RH');
INSERT INTO Utilisateur 
    VALUES ('julia@alomard.fr', 'jalomard', 'ALOMARD', '52f3eEVdF', '1984/09/05', 'F', 'France', 'La Rochelle', 
            '{"numero":95, "num_opt":null, "rue": "Rue de l aquarium"}', '["0740560210", "0165908430"]', 
            5689212, 'Chargée de Communication');


INSERT INTO Flux_de_publication 
    VALUES ('Publications Renault', '2020-01-01 04:21:00', 'antoine@beh.fr', 'interne');

-- SIMULATION CREATION EN CASCADE GROUPE (TRIGGER)

INSERT INTO Groupe_d_utilisateurs
    VALUES ('Publications Renault_Lecteur', 'Généré automatiquement', 'antoine@beh.fr', 'Publications Renault');
INSERT INTO Flux_de_publication__Groupe_d_utilisateurs
    VALUES('Publications Renault', 'Publications Renault_Lecteur');
INSERT INTO Permissions__Groupe_d_utilisateurs
    VALUES('lecteur', 'Publications Renault_Lecteur');
INSERT INTO Utilisateur__Groupe_d_utilisateurs
    VALUES ('antoine@beh.fr', 'Publications Renault_Lecteur');

INSERT INTO Groupe_d_utilisateurs
    VALUES ('Publications Renault_Redacteur', 'Généré automatiquement', 'antoine@beh.fr', 'Publications Renault');
INSERT INTO Flux_de_publication__Groupe_d_utilisateurs
    VALUES('Publications Renault', 'Publications Renault_Redacteur');
INSERT INTO Permissions__Groupe_d_utilisateurs
    VALUES('redacteur', 'Publications Renault_Redacteur');
INSERT INTO Utilisateur__Groupe_d_utilisateurs
    VALUES ('antoine@beh.fr', 'Publications Renault_Redacteur');

-- FIN SIMULATION (TRIGGER)


INSERT INTO Flux_de_publication 
    VALUES ('Mise à jour de l outil', '2020-02-11 08:26:30', 'frank@bernard.fr', 'publique');

-- SIMULATION CREATION EN CASCADE GROUPE (TRIGGER)

INSERT INTO Groupe_d_utilisateurs
    VALUES ('Mise à jour de l outil_Lecteur', 'Généré automatiquement', 'frank@bernard.fr', 'Mise à jour de l outil');
INSERT INTO Flux_de_publication__Groupe_d_utilisateurs
    VALUES('Mise à jour de l outil', 'Mise à jour de l outil_Lecteur');
INSERT INTO Permissions__Groupe_d_utilisateurs
    VALUES('lecteur', 'Mise à jour de l outil_Lecteur');
INSERT INTO Utilisateur__Groupe_d_utilisateurs
    VALUES ('frank@bernard.fr', 'Mise à jour de l outil_Lecteur');

INSERT INTO Groupe_d_utilisateurs
    VALUES ('Mise à jour de l outil_Redacteur', 'Généré automatiquement', 'frank@bernard.fr', 'Mise à jour de l outil');
INSERT INTO Flux_de_publication__Groupe_d_utilisateurs
    VALUES('Mise à jour de l outil', 'Mise à jour de l outil_Redacteur');
INSERT INTO Permissions__Groupe_d_utilisateurs
    VALUES('redacteur', 'Mise à jour de l outil_Redacteur');
INSERT INTO Utilisateur__Groupe_d_utilisateurs
    VALUES ('frank@bernard.fr', 'Mise à jour de l outil_Redacteur');

-- FIN SIMULATION (TRIGGER)


INSERT INTO Flux_de_publication 
    VALUES ('L Energie et le Monde', '2020-02-14 15:10:30', 'emma@vetson.fr', 'prive');

-- SIMULATION CREATION EN CASCADE GROUPE (TRIGGER)

INSERT INTO Groupe_d_utilisateurs
    VALUES ('L Energie et le Monde_Lecteur', 'Généré automatiquement', 'emma@vetson.fr', 'L Energie et le Monde');
INSERT INTO Flux_de_publication__Groupe_d_utilisateurs
    VALUES('L Energie et le Monde', 'L Energie et le Monde_Lecteur');
INSERT INTO Permissions__Groupe_d_utilisateurs
    VALUES('lecteur', 'L Energie et le Monde_Lecteur');
INSERT INTO Utilisateur__Groupe_d_utilisateurs
    VALUES ('emma@vetson.fr', 'L Energie et le Monde_Lecteur');

INSERT INTO Groupe_d_utilisateurs
    VALUES ('L Energie et le Monde_Redacteur', 'Généré automatiquement', 'emma@vetson.fr', 'L Energie et le Monde');
INSERT INTO Flux_de_publication__Groupe_d_utilisateurs
    VALUES('L Energie et le Monde', 'L Energie et le Monde_Redacteur');
INSERT INTO Permissions__Groupe_d_utilisateurs
    VALUES('redacteur', 'L Energie et le Monde_Redacteur');
INSERT INTO Utilisateur__Groupe_d_utilisateurs
    VALUES ('emma@vetson.fr', 'L Energie et le Monde_Redacteur');

-- FIN SIMULATION (TRIGGER)


INSERT INTO Utilisateur__Groupe_d_utilisateurs
    VALUES ('emma@vetson.fr', 'Mise à jour de l outil_Redacteur');


INSERT INTO Groupe_d_utilisateurs
    VALUES ('Interessés par l energie', 'Groupe des membres présentant un intérêt pour le secteur de l énergie.', 'emma@vetson.fr', NULL);
INSERT INTO Permissions__Groupe_d_utilisateurs
    VALUES('lecteur', 'Interessés par l energie');
INSERT INTO Permissions__Groupe_d_utilisateurs
    VALUES('voter', 'Interessés par l energie');
INSERT INTO Permissions__Groupe_d_utilisateurs
    VALUES('commenter', 'Interessés par l energie');

INSERT INTO Flux_de_publication__Groupe_d_utilisateurs
    VALUES ('L Energie et le Monde', 'Interessés par l energie');

INSERT INTO Utilisateur__Groupe_d_utilisateurs
    VALUES ('frank@bernard.fr', 'Interessés par l energie');
INSERT INTO Utilisateur__Groupe_d_utilisateurs
    VALUES ('antoine@beh.fr', 'Interessés par l energie');
INSERT INTO Utilisateur__Groupe_d_utilisateurs
    VALUES ('yasmine@loroux.fr', 'Interessés par l energie');

INSERT INTO Utilisateur__Groupe_d_utilisateurs
    VALUES ('louis@calvin.fr', 'Publications Renault_Lecteur');



INSERT INTO Publication
    VALUES (1, 'Bienvenue sur le flux Renault !', '2020-02-01 04:21:00', '2020-02-02 04:21:00', NULL, 
    'antoine@beh.fr', 'antoine@beh.fr', 'Publications Renault', 'Validé');
INSERT INTO Publication__Sujet
    VALUES (1, 'Organisation');

INSERT INTO Publication
    VALUES (2, 'Nouvel espace co-working', '2020-03-04 04:21:00', NULL, NULL, 
    'antoine@beh.fr', 'antoine@beh.fr', 'Publications Renault', 'En cours');
INSERT INTO Publication__Sujet
    VALUES (2, 'Organisation');

INSERT INTO Publication
    VALUES (3, 'Notes de changement #01', '2020-12-04 12:21:00', '2020-12-04 14:21:05', './logo.png', 
    'frank@bernard.fr', 'frank@bernard.fr', 'Mise à jour de l outil', 'Validé');
INSERT INTO Publication__Sujet
    VALUES (3, 'Application');

INSERT INTO Publication
    VALUES (4, 'Nous voulons votre avis !', '2020-12-09 05:41:41', NULL, './logo.png', 
    'frank@bernard.fr', 'frank@bernard.fr', 'Mise à jour de l outil', 'Rejeté');
INSERT INTO Publication__Sujet
    VALUES (4, 'Application');

INSERT INTO Publication
    VALUES (5, 'Cachier de doléance', '2020-12-12 05:41:41', '2020-12-13 02:27:41', './livre.png', 
    'emma@vetson.fr', 'emma@vetson.fr', 'Mise à jour de l outil', 'Validé');
INSERT INTO Publication__Sujet
    VALUES (5, 'Application');

INSERT INTO Publication
    VALUES (6, 'Comment assurer la transition energetique ?', '2020-02-14 02:27:24', '2020-02-14 07:12:08', './eolienne_barage_solaire.png', 
    'emma@vetson.fr', 'emma@vetson.fr', 'L Energie et le Monde', 'Validé');
INSERT INTO Publication__Sujet
    VALUES (6, 'Energie');

INSERT INTO Publication
    VALUES (7, 'La voiture de demain : la voiture autonome', '2020-02-21 11:09:54', '2020-02-27 11:09:56', './uber.png', 
    'antoine@beh.fr', 'antoine@beh.fr', 'Publications Renault', 'Validé');
INSERT INTO Publication__Sujet
    VALUES (7, 'Automobile');


INSERT INTO Vote
    VALUES ('antoine@beh.fr', 5, '2020-12-13 11:09:54', true);
INSERT INTO Vote
    VALUES ('frank@bernard.fr', 5, '2020-12-12 22:02:37', true);
INSERT INTO Vote
    VALUES ('yasmine@loroux.fr', 5, '2020-12-12 23:44:10', false);

INSERT INTO Commentaire
    VALUES ('antoine@beh.fr', 5, '2020-12-13 11:10:07', 'Super article ! J ai hâte de vivre dans ce monde de demain ;)');
INSERT INTO Commentaire
    VALUES ('frank@bernard.fr', 5, '2020-12-12 22:03:03', 'Pistes intéressante ... Pourriez-vous donner vos sources ?');


/* Création de la vue qui associe les utilisateurs aux publications dont ils ont le droit de lecture */
CREATE VIEW Publications_visible_par_utilisateur
AS
SELECT DISTINCT p.id_publi AS id_publi, main_user.email AS mail_user
FROM Publication p 
    INNER JOIN Flux_de_publication f ON p.publiee_dans = f.titre 
    INNER JOIN Utilisateur resp_usr ON resp_usr.email = f.responsable_flux 
    INNER JOIN Etats_possibles ep ON p.etat = ep.nom_etat
    INNER JOIN Flux_de_publication__Groupe_d_utilisateurs fgpu ON f.titre = fgpu.titre
    INNER JOIN Groupe_d_utilisateurs gpu ON gpu.nom_groupe = fgpu.nom_groupe
    INNER JOIN Utilisateur__Groupe_d_utilisateurs ugpu ON ugpu.nom_groupe = gpu.nom_groupe
    INNER JOIN Utilisateur group_member ON group_member.email = ugpu.email
    ,UTILISATEUR main_user 
WHERE ep.publi_visible = true AND
(
    f.confidentialite = 'publique' 
    OR (f.confidentialite = 'interne' AND main_user.entreprise = resp_usr.entreprise)
    OR (main_user.email = group_member.email)
)
ORDER BY main_user.email
;

/* Création de la vue calculant les scores positifs et négatifs */
CREATE VIEW Publication_score
AS
SELECT p.id_publi, v.positif, COUNT(DISTINCT v.votant) As compte
FROM Publication p
    INNER JOIN Utilisateur u ON p.redacteur = u.email
    LEFT JOIN Vote v ON v.concerne = p.id_publi
    INNER JOIN Identite_civile ic ON ic.nom = u.nom
GROUP BY p.id_publi, p.titre_publi, p.date_publi, p.chemin_vignette, p.redacteur, ic.prenom, ic.nom, v.positif;


/* Liste des groupes de l'utilisateur Frank BERNARD qui s'affiche sur le dashboard de page d'accueil */
SELECT gp.nom_groupe, gp.description_groupe
FROM Groupe_d_utilisateurs gp INNER JOIN Utilisateur__Groupe_d_utilisateurs AS usr ON gp.nom_groupe = usr.nom_groupe
WHERE usr.email = 'frank@bernard.fr';


/* Nombre de groupe dont Frank BERNARD est responsable qui s'affiche sur le dashboard de page d'accueil */
SELECT COUNT(*)
FROM Groupe_d_utilisateurs 
WHERE responsable_groupe = 'frank@bernard.fr';


/* Affichage des publications visibles par Antoine BEH triés par date de publication */
SELECT p.id_publi, p.titre_publi, p.date_publi, p.chemin_vignette, p.redacteur, ic.prenom, ic.nom
FROM Publications_visible_par_utilisateur pvu 
    INNER JOIN Publication p ON pvu.id_publi = p.id_publi
    INNER JOIN Utilisateur u ON p.redacteur = u.email
    INNER JOIN Identite_civile ic ON 
        u.nom = ic.nom
        AND
        u.date_de_naissance = ic.date_de_naissance
        AND
        u.ville = ic.ville
WHERE pvu.mail_user = 'antoine@beh.fr'
ORDER BY p.date_publi;

SELECT u.nom, tel AS "numéro"
FROM UTILISATEUR u, JSON_ARRAY_ELEMENTS(u.telephones) tel;

CREATE VIEW adresse
AS
SELECT u.email, u.numero_et_rue->>'numero' AS "numero", u.numero_et_rue->>'num_opt' AS "num_opt", u.numero_et_rue->>'rue' AS "rue"
FROM Utilisateur u;

SELECT u.nom, 
       ic.prenom,
       CONCAT(ad.numero, ' ', ad.num_opt, ' ', ad.rue) AS Adresse
FROM Utilisateur u
    INNER JOIN Identite_civile ic ON 
        u.nom = ic.nom
        AND
        u.date_de_naissance = ic.date_de_naissance
        AND
        u.ville = ic.ville
    INNER JOIN Adresse ad ON u.email = ad.email;